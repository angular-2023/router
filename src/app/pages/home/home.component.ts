import { Component, OnInit } from "@angular/core";
import { Product } from "src/app/models/product";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  products: Product[] = [
    {
      id: 1,
      name: "Canapé jaune",
      cover: "assets/canape-jaune.png",
      details: "Livraison sous 2 semaines",
      price: 799,
    },
    {
      id: 2,
      name: "Chaise bois",
      cover: "assets/chaise-bois.png",
      details: "Livraison sous 3 jours",
      price: 299,
      discountedPrice: 99,
    },
    {
      id: 3,
      name: "Fauteuil jaune",
      cover: "assets/fauteuil-jaune.png",
      details: "Rupture de stock",
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
