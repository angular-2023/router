import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { Product } from "src/app/models/product";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"],
})
export class ProductComponent implements OnInit {
  product: Product;
  constructor(private location: Location) {}

  ngOnInit(): void {
    this.product = this.location.getState() as Product;
  }
}
