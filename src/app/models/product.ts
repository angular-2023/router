export interface Product {
  id: number;
  cover: string;
  name: string;
  details: string;
  price?: number;
  discountedPrice?: number;
}
