import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { ProductComponent } from "./pages/product/product.component";
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { AboutComponent } from "./pages/about/about.component";
import { CanAccessGuard } from "./guards/can-access.guard";
import { CantAccessGuard } from "./guards/cant-access.guard";

const routes: Routes = [
  {
    path: "home",
    component: HomeComponent,
  },
  {
    path: "product/:id",
    component: ProductComponent,
    canActivate: [CanAccessGuard],
  },
  {
    path: "about",
    component: AboutComponent,
  },
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full",
  },
  {
    path: "**",
    component: NotFoundComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
