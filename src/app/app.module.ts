import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { ProductItemComponent } from "./component/product-item/product-item.component";
import { AppRoutingModule } from "./app-routing.module";
import { ProductComponent } from "./pages/product/product.component";
import { HomeComponent } from "./pages/home/home.component";
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { AboutComponent } from "./pages/about/about.component";
import { NavbarComponent } from "./component/navbar/navbar.component";

@NgModule({
  declarations: [
    AppComponent,
    ProductItemComponent,
    ProductComponent,
    HomeComponent,
    NotFoundComponent,
    AboutComponent,
    NavbarComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
